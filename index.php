<?php session_start();
$site_url = 'http://ua-tuning.com/';
if (array_key_exists('utm_referrer', $_GET)) {
  if (strpos($site_url, $_GET['utm_referrer'])===false) {
    $_SESSION['referer'] = $_GET['utm_referrer'];
  }
}
if (array_key_exists('utm_source', $_GET)) {
  if (strpos($site_url, $_GET['utm_source'])===false) {
    $_SESSION['sourse'] = $_GET['utm_source'];
  }
  if (strpos($site_url, $_GET['utm_term'])===false) {
    $_SESSION['term'] = $_GET['utm_term'];
  }
  if (strpos($site_url, $_GET['utm_campaign'])===false) {
    $_SESSION['campaign'] = $_GET['utm_campaign'];
  }
}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <title>UA-Tuning</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="js/libs/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="js/libs/flipster/jquery.flipster.min.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
  <script src="js/libs/jquery.min.js"></script>
  <script src="js/libs/jquery-ui/jquery-ui.min.js"></script>
  <script src="js/libs/maskedinput.min.js"></script>
  <script src="js/libs/validate/jquery.validate.min.js"></script>
  <script src="js/libs/fancybox/jquery.fancybox.pack.js"></script>
  <!-- <script src="js/tabs.js"></script> -->
  <script src="js/libs/slick.min.js"></script>
  <script src="js/libs/flipster/jquery.flipster.min.js"></script>
  <script src="js/libs/jquery.lazyload.min.js"></script>
  <script src="/js/livesearch.js" charset="utf-8"></script>
</head>

<body>
<!-- Google Tag Manager -->
<script>dataLayer=[];</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MQZGM3"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MQZGM3');</script>
<!-- End Google Tag Manager -->
<div id="page-preloader">
  <div class="body">
  	<span>
      	<span></span>
      	<span></span>
      	<span></span>
      	<span></span>
    </span>
    <div class="base">
        <span></span>
        <div class="face"></div>
    </div>
</div>
<div class="longfazers">
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>
<h1>Секундочку...</h1>
</div>


<section class="wrapper scr-i__wrapper">
  <header class="inner header">
    <img class="header__logo" src="img/logo.png" alt="El Auto">
    <div class="header__buttonbox" tabindex="1">
      <div class="header__buttonbox__button" data-content="Заказать звонок" data-init="modal"
           data-modal="#callback-modal"  data-order="Заказ обратного звонка (шапка сайта)" data-hidden="/callback-header.html"></div>
      Работаем без выходных
    </div>
      <span class="header__phone">
        <a class="header__link" href="tel:+380503232740">+38 (050) 323-27-40</a>
      </span>
  </header>
  <div class="inner scr-i">
      <span class="scr-i__title" id="utm-title">
        Качественный обвес из нержавейки<br>
        для надежной защиты вашего автомобиля
      </span>
      <span class="scr-i__subtitle">
        Доставка по всей Украине за 2 дня
      </span>
    <div class="scr-i__form-wrapper">
        <span class="scr-i__form-title">
          Подберите защиту
        </span>
        <span class="scr-i__form-subtitle">
          Более 25 видов на каждую марку авто
        </span>
      <div class="scr-i__form">
        <form action="sendmessage.php" id="form-1" method="POST">
          <div class="scr-i__input-wrapper">
            <input type="text" class="scr-i__input" placeholder="Введите марку авто" name="auto">
          </div>
          <div class="scr-i__input-wrapper">
            <input type="text" class="scr-i__input" placeholder="Введите телефон" name="phone">
          </div>
          <input type="hidden" name="order" value="Первый экран-форма">
          <input type="hidden" name="tagmanager" value="/first-screen-form.html">
          <div class="scr-i__input-wrapper">
            <button class="scr-i__button" type="submit" data-content="Подобрать сейчас"></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
  <section class="scr-ii__wrapper wrapper" id="after">
    <div class="scr-ii__inner inner">
      <div id="frontIcon" class="scr-ii__egg">
        <img src="img/egg11.png" alt="">
        <p>Защита переднего бампера</p>
      </div>
      <div id="sidewall" class="scr-ii__egg">
        <img src="img/egg22.png" alt="">
        <p>Боковая защита и пороги</p>
      </div>
      <div id="rear" class="scr-ii__egg">
        <img src="img/egg33.png" alt="">
        <p>Защита заднего бампера</p>
      </div>
      <div id="Roof" class="scr-ii__egg">
        <img src="img/egg44.png" alt="">
        <p>Рейлинги и перемычки</p>
      </div>
      <div id="korch" class="scr-ii__egg">
        <img src="img/egg55.png" alt="">
        <p>Аксессуары для ВАЗ</p>
      </div>
    </div>
  </section>


  <section class="scr-front__wrapper wrapper" id="front" data-utm="peredniy_bamper_poisk">
    <div class="scr-front__triangle"></div>
    <div class="scr-front__inner inner">
      <div class="scr-front__title">Защита переднего бампера</div>
      <div id="carousel-front">
        <ul class="flip-items">
          <!--<li data-price=155" data-flip-category="Кенгурятник высокий">-->
            <!--<img class="lazy" data-original="img/product/Front/KengH/02.JPG">-->
          <!--</li>-->
          <li data-price="155" data-flip-category="Кенгурятник высокий">
            <img src="img/product/Front/KengH/71.jpg">
          </li>
          <li data-price="155" data-flip-category="Кенгурятник высокий">
            <img src="img/product/Front/KengH/72.jpg">
          </li>
          <li data-price="155" data-flip-category="Кенгурятник высокий">
            <img src="img/product/Front/KengH/73.JPG">
          </li>
          <li data-price="155" data-flip-category="Кенгурятник высокий">
            <img src="img/product/Front/KengH/74.jpg">
          </li>
          <li data-price="155" data-flip-category="Кенгурятник высокий">
            <img src="img/product/Front/KengH/75.jpg">
          </li>
          <li data-price="155" data-flip-category="Кенгурятник высокий">
            <img src="img/product/Front/KengH/76.JPG">
          </li>
          <li data-price="145" data-flip-category="Кенгурятник низкий">
            <img src="img/product/Front/KengL/77.JPG">
          </li>
          <li data-price="145" data-flip-category="Кенгурятник низкий">
            <img src="img/product/Front/KengL/78.JPG">
          </li>
          <li data-price="145" data-flip-category="Кенгурятник низкий">
            <img src="img/product/Front/KengL/79.JPG">
          </li>
          <li data-price="145" data-flip-category="Кенгурятник низкий">
            <img src="img/product/Front/KengL/80.jpg">
          </li>
          <li data-price="145" data-flip-category="Кенгурятник низкий">
            <img src="img/product/Front/KengL/81.JPG">
          </li>
          <li data-price="145" data-flip-category="Кенгурятник низкий">
            <img src="img/product/Front/KengL/82.JPG">
          </li>
          <li data-price="90" data-flip-category="Труба одинарная">
            <img src="img/product/Front/solo/95.jpg">
          </li>
          <li data-price="90" data-flip-category="Труба одинарная">
            <img src="img/product/Front/solo/96.jpg">
          </li>
          <li data-price="90" data-flip-category="Труба одинарная">
            <img src="img/product/Front/solo/97.jpg">
          </li>
          <li data-price="90" data-flip-category="Труба одинарная">
            <img src="img/product/Front/solo/98.JPG">
          </li>
          <li data-price="90" data-flip-category="Труба одинарная">
            <img src="img/product/Front/solo/99.jpg">
          </li>
          <li data-price="90" data-flip-category="Труба изогнутая">
            <img src="img/product/Front/soloFlex/90.jpg">
          </li>
          <li data-price="90" data-flip-category="Труба изогнутая">
            <img src="img/product/Front/soloFlex/91.jpg">
          </li>
          <li data-price="90" data-flip-category="Труба изогнутая">
            <img src="img/product/Front/soloFlex/92.JPG">
          </li>
          <li data-price="90" data-flip-category="Труба изогнутая">
            <img src="img/product/Front/soloFlex/93.JPG">
          </li>
          <li data-price="90" data-flip-category="Труба изогнутая">
            <img src="img/product/Front/soloFlex/94.JPG">
          </li>
          <li data-price="130" data-flip-category="Труба двойная">
            <img src="img/product/Front/dobl/83.jpg">
          </li>
          <li data-price="130" data-flip-category="Труба двойная">
            <img src="img/product/Front/dobl/84.jpg">
          </li>
          <li data-price="130" data-flip-category="Труба двойная">
            <img src="img/product/Front/dobl/85.jpg">
          </li>
          <li data-price="130" data-flip-category="Труба двойная">
            <img src="img/product/Front/dobl/86.jpg">
          </li>
          <li data-price="130" data-flip-category="Труба двойная">
            <img src="img/product/Front/dobl/87.jpg">
          </li>
          <li data-price="130" data-flip-category="Труба двойная">
            <img src="img/product/Front/dobl/88.JPG">
          </li>
          <li data-price="145" data-flip-category="Труба с грилем">
            <img src="img/product/Front/grill/100.jpg">
          </li>
          <li data-price="145" data-flip-category="Труба с грилем">
            <img src="img/product/Front/grill/101.JPG">
          </li>
          <li data-price="145" data-flip-category="Труба с грилем">
            <img src="img/product/Front/grill/102.jpg">
          </li>
          <li data-price="145" data-flip-category="Труба с грилем">
            <img src="img/product/Front/grill/103.jpg">
          </li>
          <li data-price="145" data-flip-category="Труба с грилем">
            <img src="img/product/Front/grill/104.jpg">
          </li>
          <li data-price="145" data-flip-category="Труба с грилем">
            <img src="img/product/Front/grill/105.JPG">
          </li>
        </ul>
      </div>
      <div class="scr-i__button look" data-content="Подобрать сейчас"></div>
      <script>
        var carousel = $("#carousel-front").flipster({
          style: 'carousel',
          spacing: -0.5,
          nav: true,
          buttons: true,
          scrollwheel: false,
          start: 0
        });
      </script>

    </div>
  </section>

  <section class="scr-form__wrapper wrapper">
    <div class="scr-form__inner inner">
      <div class="scr-form__title">Подберем под ваш автомобиль</div>
      <form  action="sendmessage.php" method="POST">
        <div class="scr-form__input-wrapper">
          <input type="text" class="scr-form__input" placeholder="Введите марку авто" name="auto">
        </div>
        <div class="scr-form__input-wrapper">
          <input type="text" class="scr-form__input" placeholder="Введите телефон" name="phone">
        </div>
        <input type="hidden" name="order" value="Форма после(Защита переднего бампера)">
        <input type="hidden" name="tagmanager" value="/form-after-product-slider.html">
        <div class="scr-form__input-wrapper">
          <button class="scr-form__button" type="submit" data-content="Подобрать сейчас"></button>
        </div>
      </form>
    </div>
  </section>



  <section class="scr-front__wrapper wrapper" id="side" data-utm="bokovaya_zaschita_poisk">
    <div class="scr-front__inner inner">
      <div class="scr-front__title">Боковая защита</div>
        <div id="carousel-side">
          <ul class="flip-items">
            <li data-price="120" data-flip-category="Площадка с листом из нержавейки 42d">
              <img src="img/product/Side/nerz42/7.jpg">
            </li>
            <li data-price="120" data-flip-category="Площадка с листом из нержавейки 42d">
              <img src="img/product/Side/nerz42/8.JPG">
            </li>
            <li data-price="120" data-flip-category="Площадка с листом из нержавейки 42d">
              <img src="img/product/Side/nerz42/9.JPG">
            </li>
            <li data-price="120" data-flip-category="Площадка с листом из нержавейки 42d">
              <img src="img/product/Side/nerz42/10.jpg">
            </li>
            <li data-price="120" data-flip-category="Площадка с листом из нержавейки 42d">
              <img src="img/product/Side/nerz42/11.JPG">
            </li>
            <li data-price="120" data-flip-category="Площадка с листом из нержавейки 42d">
              <img src="img/product/Side/nerz42/12.jpeg">
            </li>
            <li data-price="150" data-flip-category="Площадка с листом из нержавейки 60d">
              <img src="img/product/Side/nerz60/13.jpg">
            </li>
            <li data-price="150" data-flip-category="Площадка с листом из нержавейки 60d">
              <img src="img/product/Side/nerz60/14.JPG">
            </li>
            <li data-price="150" data-flip-category="Площадка с листом из нержавейки 60d">
              <img src="img/product/Side/nerz60/15.JPG">
            </li>
            <li data-price="150" data-flip-category="Площадка с листом из нержавейки 60d">
              <img src="img/product/Side/nerz60/16.JPG">
            </li>
            <li data-price="150" data-flip-category="Площадка с листом из нержавейки 60d">
              <img src="img/product/Side/nerz60/17.JPG">
            </li>
            <li data-price="175" data-flip-category="Площадка с листом из нержавейки с загибом 60d">
              <img src="img/product/Side/plosh_nerzh_zagib60/18.JPG">
            </li>
            <li data-price="175" data-flip-category="Площадка с листом из нержавейки с загибом 60d">
              <img src="img/product/Side/plosh_nerzh_zagib60/19.JPG">
            </li>
            <li data-price="175" data-flip-category="Площадка с листом из нержавейки с загибом 60d">
              <img src="img/product/Side/plosh_nerzh_zagib60/20.JPG">
            </li>
            <li data-price="175" data-flip-category="Площадка с листом из нержавейки с загибом 60d">
              <img src="img/product/Side/plosh_nerzh_zagib60/21.jpg">
            </li>
            <li data-price="175" data-flip-category="Площадка с листом из нержавейки с загибом 60d">
              <img src="img/product/Side/plosh_nerzh_zagib60/22.JPG">
            </li>
            <li data-price="175" data-flip-category="Площадка с листом из нержавейки с загибом 60d">
              <img src="img/product/Side/plosh_nerzh_zagib60/23.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба 70d">
              <img src="img/product/Side/tube70/24.jpg">
            </li>
            <li data-price="135" data-flip-category="Труба 70d">
              <img src="img/product/Side/tube70/25.jpg">
            </li>
            <li data-price="135" data-flip-category="Труба 70d">
              <img src="img/product/Side/tube70/26.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба 70d">
              <img src="img/product/Side/tube70/27.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба 70d">
              <img src="img/product/Side/tube70/28.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба 70d">
              <img src="img/product/Side/tube70/29.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба с проступью 70d">
              <img src="img/product/Side/tube_prost70/30.jpg">
            </li>
            <li data-price="135" data-flip-category="Труба с проступью 70d">
              <img src="img/product/Side/tube_prost70/31.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба с проступью 70d">
              <img src="img/product/Side/tube_prost70/32.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба с проступью 70d">
              <img src="img/product/Side/tube_prost70/33.jpg">
            </li>
            <li data-price="135" data-flip-category="Труба с проступью 70d">
              <img src="img/product/Side/tube_prost70/34.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба с проступью 70d">
              <img src="img/product/Side/tube_prost70/35.JPG">
            </li>
            <li data-price="135" data-flip-category="Труба с проступью 70d">
              <img src="img/product/Side/tube_prost70/36.JPG">
            </li>
            <li data-price="150" data-flip-category="Алюминиевые пороги">
              <img src="img/product/Side/Alumin/1.jpeg">
            </li>
            <li data-price="150" data-flip-category="Алюминиевые пороги">
              <img src="img/product/Side/Alumin/2.JPG">
            </li>
            <li data-price="150" data-flip-category="Алюминиевые пороги">
              <img src="img/product/Side/Alumin/3.JPG">
            </li>
            <li data-price="150" data-flip-category="Алюминиевые пороги">
              <img src="img/product/Side/Alumin/4.jpg">
            </li>
            <li data-price="150" data-flip-category="Алюминиевые пороги">
              <img src="img/product/Side/Alumin/5.jpg">
            </li>
            <li data-price="150" data-flip-category="Алюминиевые пороги">
              <img src="img/product/Side/Alumin/6.JPG">
            </li>
          </ul>
        </div>
      <div class="scr-i__button look" data-content="Подобрать сейчас"></div>
        <script>
          var carousel = $("#carousel-side").flipster({
            style: 'carousel',
            spacing: -0.5,
            nav: true,
            buttons:   true,
            scrollwheel: false,
            start:0,
          });
        </script>

    </div>
  </section>


  <section class="scr-form__wrapper wrapper">
    <div class="scr-form__inner inner">
      <div class="scr-form__title">Подберем под ваш автомобиль</div>
      <form  action="sendmessage.php" method="POST">
        <div class="scr-form__input-wrapper">
          <input type="text" class="scr-form__input" placeholder="Введите марку авто" name="auto">
        </div>
        <div class="scr-form__input-wrapper">
          <input type="text" class="scr-form__input" placeholder="Введите телефон" name="phone">
        </div>
        <input type="hidden" name="order" value="Форма после(Боковая защита)">
        <input type="hidden" name="tagmanager" value="/form-after-product-slider.html">
        <div class="scr-form__input-wrapper">
          <button class="scr-form__button" type="submit" data-content="Подобрать сейчас"></button>
        </div>
      </form>
    </div>
  </section>

<section class="scr-front__wrapper wrapper" id="back" data-utm="zadniy_bamper_poisk">
  <div class="scr-front__inner inner">
    <div class="scr-front__title">Защита заднего бампера</div>
    <div id="carousel-back">
      <ul class="flip-items">
        <li data-price="90" data-flip-category="Прямая труба">
          <img src="img/product/Back/straight/49.JPG">
        </li>
        <li data-price="90" data-flip-category="Прямая труба">
          <img src="img/product/Back/straight/50.JPG">
        </li>
        <li data-price="90" data-flip-category="Прямая труба">
          <img src="img/product/Back/straight/51.JPG">
        </li>
        <li data-price="90" data-flip-category="Прямая труба">
          <img src="img/product/Back/straight/52.JPG">
        </li>
        <li data-price="90" data-flip-category="Прямая труба">
          <img src="img/product/Back/straight/53.JPG">
        </li>
        <li data-price="90" data-flip-category="Прямая труба">
          <img src="img/product/Back/straight/54.JPG">
        </li>
        <li data-price="90" data-flip-category="Изогнутая труба">
          <img src="img/product/Back/flex/66.jpg">
        </li>
        <li data-price="90" data-flip-category="Изогнутая труба">
          <img src="img/product/Back/flex/67.jpg">
        </li>
        <li data-price="90" data-flip-category="Изогнутая труба">
          <img src="img/product/Back/flex/68.JPG">
        </li>
        <li data-price="90" data-flip-category="Изогнутая труба">
          <img src="img/product/Back/flex/69.JPG">
        </li>
        <li data-price="90" data-flip-category="Изогнутая труба">
          <img src="img/product/Back/flex/70.JPG">
        </li>
        <li data-price="125" data-flip-category="Уголки двойные">
          <img src="img/product/Back/doble/55.jpg">
        </li>
        <li data-price="125" data-flip-category="Уголки двойные">
          <img src="img/product/Back/doble/56.jpg">
        </li>
        <li data-price="125" data-flip-category="Уголки двойные">
          <img src="img/product/Back/doble/57.JPG">
        </li>
        <li data-price="125" data-flip-category="Уголки двойные">
          <img src="img/product/Back/doble/58.jpg">
        </li>
        <li data-price="125" data-flip-category="Уголки двойные">
          <img src="img/product/Back/doble/59.JPG">
        </li>
        <li data-price="125" data-flip-category="Уголки двойные">
          <img src="img/product/Back/doble/60.jpg">
        </li>
        <li data-price="90" data-flip-category="Уголки одинарные">
          <img src="img/product/Back/solo/61.JPG">
        </li>
        <li data-price="90" data-flip-category="Уголки одинарные">
          <img src="img/product/Back/solo/62.JPG">
        </li>
        <li data-price="90" data-flip-category="Уголки одинарные">
          <img src="img/product/Back/solo/63.JPG">
        </li>
        <li data-price="90" data-flip-category="Уголки одинарные">
          <img src="img/product/Back/solo/64.JPG">
        </li>
        <li data-price="90" data-flip-category="Уголки одинарные">
          <img src="img/product/Back/solo/65.jpg">
        </li>
        <li data-price="90" data-flip-category="U-образная труба">
          <img src="img/product/Back/U/43.JPG">
        </li>
        <li data-price="90" data-flip-category="U-образная труба">
          <img src="img/product/Back/U/44.jpg">
        </li>
        <li data-price="90" data-flip-category="U-образная труба">
          <img src="img/product/Back/U/45.JPG">
        </li>
        <li data-price="90" data-flip-category="U-образная труба">
          <img src="img/product/Back/U/46.JPG">
        </li>
        <li data-price="90" data-flip-category="U-образная труба">
          <img src="img/product/Back/U/47.jpg">
        </li>
        <li data-price="90" data-flip-category="U-образная труба">
          <img src="img/product/Back/U/48.jpg">
        </li>
        <li data-price="90" data-flip-category="ML-образная труба">
          <img src="img/product/Back/ml/37.JPG">
        </li>
        <li data-price="90" data-flip-category="ML-образная труба">
          <img src="img/product/Back/ml/38.JPG">
        </li>
        <li data-price="90" data-flip-category="ML-образная труба">
          <img src="img/product/Back/ml/39.JPG">
        </li>
        <li data-price="90" data-flip-category="ML-образная труба">
          <img src="img/product/Back/ml/40.jpg">
        </li>
        <li data-price="90" data-flip-category="ML-образная труба">
          <img src="img/product/Back/ml/41.jpg">
        </li>
        <li data-price="90" data-flip-category="ML-образная труба">
          <img src="img/product/Back/ml/42.jpg">
        </li>
      </ul>
    </div>
    <div class="scr-i__button look" data-content="Подобрать сейчас"></div>
    <script>
      var carousel = $("#carousel-back").flipster({
        style: 'carousel',
        spacing: -0.5,
        nav: true,
        buttons: true,
        scrollwheel: false,
        start: 0,
      });
    </script>

  </div>
</section>

<section class="scr-form__wrapper wrapper">
  <div class="scr-form__inner inner">
    <div class="scr-form__title">Подберем под ваш автомобиль</div>
    <form  action="sendmessage.php" method="POST">
      <div class="scr-form__input-wrapper">
        <input type="text" class="scr-form__input" placeholder="Введите марку авто" name="auto">
      </div>
      <div class="scr-form__input-wrapper">
        <input type="text" class="scr-form__input" placeholder="Введите телефон" name="phone">
      </div>
      <input type="hidden" name="order" value="Форма после(Защита заднего бампера)">
      <input type="hidden" name="tagmanager" value="/form-after-product-slider.html">
      <div class="scr-form__input-wrapper">
        <button class="scr-form__button" type="submit" data-content="Подобрать сейчас"></button>
      </div>
    </form>
  </div>
</section>

<section class="scr-front__wrapper wrapper" id="rails" data-utm="reilingi_poisk">
  <div class="scr-front__inner inner">
    <div class="scr-front__title">Рейлинги и перемычки</div>
    <div id="carousel-rails">
      <ul class="flip-items">
        <li data-price="45" data-flip-category="Рейлинги Can Coruma">
          <img src="img/product/Rails/Coruma/115.jpg">
        </li>
        <li data-price="45" data-flip-category="Рейлинги Can Coruma">
          <img src="img/product/Rails/Coruma/116.JPG">
        </li>
        <li data-price="45" data-flip-category="Рейлинги Can Coruma">
          <img src="img/product/Rails/Coruma/117.JPG">
        </li>
        <li data-price="45" data-flip-category="Рейлинги Can Coruma">
          <img src="img/product/Rails/Coruma/118.JPG">
        </li>
        <li data-price="45" data-flip-category="Рейлинги Can Coruma">
          <img src="img/product/Rails/Coruma/119.JPG">
        </li>
        <li data-price="65" data-flip-category="Рейлинги Crown">
          <img src="img/product/Rails/Crown/120.jpg">
        </li>
        <li data-price="65" data-flip-category="Рейлинги Crown">
          <img src="img/product/Rails/Crown/121.JPG">
        </li>
        <li data-price="65" data-flip-category="Рейлинги Crown">
          <img src="img/product/Rails/Crown/122.jpg">
        </li>
        <li data-price="65" data-flip-category="Рейлинги Crown">
          <img src="img/product/Rails/Crown/123.JPG">
        </li>
        <li data-price="65" data-flip-category="Рейлинги Crown">
          <img src="img/product/Rails/Crown/124.JPG">
        </li>
        <li data-price="75" data-flip-category="Перемычки Gold">
          <img src="img/product/Rails/Gold/106.jpg">
        </li>
        <li data-price="75" data-flip-category="Перемычки Gold">
          <img src="img/product/Rails/Gold/107.jpg">
        </li>
        <li data-price="75" data-flip-category="Перемычки Gold">
          <img src="img/product/Rails/Gold/108.jpg">
        </li>
        <li data-price="75" data-flip-category="Перемычки Gold">
          <img src="img/product/Rails/Gold/109.jpg">
        </li>
        <li data-price="75" data-flip-category="Перемычки Wing">
          <img src="img/product/Rails/Wine/110.JPG">
        </li>
        <li data-price="75" data-flip-category="Перемычки Wing">
          <img src="img/product/Rails/Wine/111.JPG">
        </li>
        <li data-price="75" data-flip-category="Перемычки Wing">
          <img src="img/product/Rails/Wine/112.JPG">
        </li>
        <li data-price="75" data-flip-category="Перемычки Wing">
          <img src="img/product/Rails/Wine/113.JPG">
        </li>
        <li data-price="75" data-flip-category="Перемычки Wing">
          <img src="img/product/Rails/Wine/114.JPG">
        </li>
        <li data-price="175" data-flip-category="Рейлинги для пикапов">
          <img src="img/product/Rails/railingi_kungi/rk1.JPG">
        </li>
        <li data-price="175" data-flip-category="Рейлинги для пикапов">
          <img src="img/product/Rails/railingi_kungi/rk2.jpg">
        </li>
        <li data-price="175" data-flip-category="Рейлинги для пикапов">
          <img src="img/product/Rails/railingi_kungi/rk3.JPG">
        </li>
        <li data-price="175" data-flip-category="Рейлинги для пикапов">
          <img src="img/product/Rails/railingi_kungi/rk4.jpg">
        </li>
      </ul>
    </div>
    <div class="scr-i__button look" data-content="Подобрать сейчас"></div>
    <script>
      var carousel = $("#carousel-rails").flipster({
        style: 'carousel',
        spacing: -0.5,
        nav: true,
        buttons: true,
        scrollwheel: false,
        start: 0,
      });
    </script>

  </div>
</section>

<section class="scr-form__wrapper wrapper">
  <div class="scr-form__inner inner">
    <div class="scr-form__title">Подберем под ваш автомобиль</div>
    <form  action="sendmessage.php" method="POST">
      <div class="scr-form__input-wrapper">
        <input type="text" class="scr-form__input" placeholder="Введите марку авто" name="auto">
      </div>
      <div class="scr-form__input-wrapper">
        <input type="text" class="scr-form__input" placeholder="Введите телефон" name="phone">
      </div>
      <input type="hidden" name="order" value="Форма после(Рейлинги и перемычки)">
      <input type="hidden" name="tagmanager" value="/form-after-product-slider.html">
      <div class="scr-form__input-wrapper">
        <button class="scr-form__button" type="submit" data-content="Подобрать сейчас"></button>
      </div>
    </form>
  </div>
</section>

<section class="scr-front__wrapper wrapper" id="black" data-utm="vazy_poisk">
  <div class="scr-front__inner inner">
    <div class="scr-front__title">Черная серия</div>
    <div id="carousel-black">
      <ul class="flip-items">
        <li data-flip-category="Пороги">
          <img src="img/product/Black/side/135.jpg">
          <div class="dollar">от 750 грн.</div>
        </li>
        <li data-flip-category="Пороги">
          <img src="img/product/Black/side/136.jpg">
          <div class="dollar">от 750 грн.</div>
        </li>
        <li data-flip-category="Пороги">
          <img src="img/product/Black/side/137.JPG">
          <div class="dollar">от 750 грн.</div>
        </li>
        <li data-flip-category="Пороги">
          <img src="img/product/Black/side/138.jpg">
          <div class="dollar">от 750 грн.</div>
        </li>
        <li data-flip-category="Кенгурятники">
          <img src="img/product/Black/Front/129.JPG">
          <div class="dollar">от 500 грн.</div>
        </li>
        <li data-flip-category="Кенгурятники">
          <img src="img/product/Black/Front/130.jpg">
          <div class="dollar">от 500 грн.</div>
        </li>
        <li data-flip-category="Кенгурятники">
          <img src="img/product/Black/Front/131.JPG">
          <div class="dollar">от 500 грн.</div>
        </li>
        <li data-flip-category="Кенгурятники">
          <img src="img/product/Black/Front/132.jpg">
          <div class="dollar">от 500 грн.</div>
        </li>
        <li data-flip-category="Кенгурятники">
          <img src="img/product/Black/Front/133.jpg">
          <div class="dollar">от 500 грн.</div>
        </li>
        <li data-flip-category="Кенгурятники">
          <img src="img/product/Black/Front/134.jpg">
          <div class="dollar">от 500 грн.</div>
        </li>
        <li data-flip-category="Задняя защита">
          <img src="img/product/Black/Back/551.jpg">
          <div class="dollar">от 800 грн.</div>
        </li>
        <li data-flip-category="Задняя защита">
          <img src="img/product/Black/Back/552.jpg">
          <div class="dollar">от 800 грн.</div>
        </li>
        <li data-flip-category="Задняя защита">
          <img src="img/product/Black/Back/127.jpg">
          <div class="dollar">от 800 грн.</div>
        </li>
        <li data-flip-category="Задняя защита">
          <img src="img/product/Black/Back/128.jpg">
          <div class="dollar">от 800 грн.</div>
        </li>
      </ul>
    </div>
    <div class="scr-i__button look" data-content="Подобрать сейчас"></div>
    <script>
      var carousel = $("#carousel-black").flipster({
        style: 'carousel',
        spacing: -0.5,
        nav: true,
        buttons: true,
        scrollwheel: false,
        start: 0,
      });
    </script>

  </div>
</section>

<section class="scr-form__wrapper wrapper">
  <div class="scr-form__inner inner">
    <div class="scr-form__title">Подберем под ваш автомобиль</div>
    <form  action="sendmessage.php" method="POST">
      <div class="scr-form__input-wrapper">
        <input type="text" class="scr-form__input" placeholder="Введите марку авто" name="auto">
      </div>
      <div class="scr-form__input-wrapper">
        <input type="text" class="scr-form__input" placeholder="Введите телефон" name="phone">
      </div>
      <input type="hidden" name="order" value="Форма после(Черная серия)">
      <input type="hidden" name="tagmanager" value="/form-after-product-slider.html">
      <div class="scr-form__input-wrapper">
        <button class="scr-form__button" type="submit" data-content="Подобрать сейчас"></button>
      </div>
    </form>
  </div>
</section>

<section class="scr-front__wrapper wrapper" data-utm="farkop_poisk">
  <div class="scr-front__inner inner">
    <div class="scr-front__title">Фаркопы</div>
    <div id="carousel-farkop">
      <ul class="flip-items">
        <li data-flip-category="Фаркопы">
          <img src="img/product/Farcop/f1.jpg">
          <div class="dollar">от 1400 грн.</div>
        </li>
        <li data-flip-category="Фаркопы">
          <img src="img/product/Farcop/f2.jpg">
          <div class="dollar">от 1400 грн.</div>
        </li>
        <li data-flip-category="Фаркопы">
          <img src="img/product/Farcop/f3.jpg">
          <div class="dollar">от 1400 грн.</div>
        </li>
        <li data-flip-category="Фаркопы">
          <img src="img/product/Farcop/f4.JPG">
          <div class="dollar">от 1400 грн.</div>
        </li>
        <li data-flip-category="Фаркопы">
          <img src="img/product/Farcop/f5.jpg">
          <div class="dollar">от 1400 грн.</div>
        </li>
        <li data-flip-category="Фаркопы">
          <img src="img/product/Farcop/f6.jpg">
          <div class="dollar">от 1400 грн.</div>
        </li>
        <li data-flip-category="Фаркопы">
          <img src="img/product/Farcop/f7.JPG">
          <div class="dollar">от 1400 грн.</div>
        </li>
      </ul>
    </div>
    <div class="scr-i__button look" data-content="Подобрать сейчас"></div>
    <script>
      var carousel = $("#carousel-farkop").flipster({
        style: 'carousel',
        spacing: -0.5,
        nav: false,
        buttons: true,
        scrollwheel: false,
        start: 0,
      });
    </script>

  </div>
</section>

<section class="scr-form__wrapper wrapper">
  <div class="scr-form__inner inner">
    <div class="scr-form__title">Подберем под ваш автомобиль</div>
    <form  action="sendmessage.php" method="POST">
      <div class="scr-form__input-wrapper">
        <input type="text" class="scr-form__input" placeholder="Введите марку авто" name="auto">
      </div>
      <div class="scr-form__input-wrapper">
        <input type="text" class="scr-form__input" placeholder="Введите телефон" name="phone">
      </div>
      <input type="hidden" name="order" value="Форма после(Фаркопы)">
      <input type="hidden" name="tagmanager" value="/form-after-product-slider.html">
      <div class="scr-form__input-wrapper">
        <button class="scr-form__button" type="submit" data-content="Подобрать сейчас"></button>
      </div>
    </form>
  </div>
</section>

  <!--<section class="scr-video__wrapper wrapper">-->
    <!--<div class="scr-video__inner inner">-->
      <!--<div class="scr-video__title">Видео-обзор нашей продукции</div>-->
      <!--<div id="carousel-video">-->
        <!--<ul class="flip-items">-->
          <!--<li>-->
            <!--<iframe src="https://player.vimeo.com/video/156537534" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->
          <!--</li>-->
          <!--<li>-->
            <!--<iframe src="https://player.vimeo.com/video/156537534" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->
          <!--</li>-->
          <!--<li>-->
            <!--<iframe src="https://player.vimeo.com/video/156537534" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->
          <!--</li>-->
        <!--</ul>-->
      <!--</div>-->

      <!--<script>-->
        <!--var carousel = $("#carousel-video").flipster({-->
          <!--style: 'carousel',-->
          <!--spacing: -0.5,-->
          <!--buttons:   true,-->
          <!--scrollwheel: false-->
        <!--});-->
      <!--</script>-->
    <!--</div>-->
  <!--</section>-->

  <section class="scr-form2__wrapper wrapper">
    <div class="scr-form2__inner inner">
      <div class="scr-form2__title">Подобрать под ваш автомобиль</div>
      <form  action="sendmessage.php" method="POST">
        <div class="scr-form2__input-wrapper">
          <input type="text" class="scr-form2__input" placeholder="Введите марку авто" name="auto">
        </div>
        <div class="scr-form2__input-wrapper">
          <input type="text" class="scr-form2__input" placeholder="Введите телефон" name="phone">
        </div>
        <input type="hidden" name="order" value="Форма перед блоком 'Как заказать'">
        <input type="hidden" name="tagmanager" value="/how-to-order.html">
        <div class="scr-form2__input-wrapper">
          <button id="12123354858" class="scr-form2__button" type="submit" data-content="Подобрать сейчас"></button>
        </div>
      </form>
    </div>
  </section>

  <section class="scr-how__wrapper wrapper">
    <div class="scr-how__inner inner">
      <div class="scr-how__title">Как заказать?</div>
      <div class="scr-how__img"></div>
    </div>
  </section>

  <section class="scr-sklad__wrapper wrapper">
    <div class="scr-sklad__inner inner">
      <div class="scr-sklad__title">

      </div>
      <div class="scr-sklad__col">
        <div class="slider_sklad">
          <img src="img/sklad/s01.jpg" alt="">
          <img src="img/sklad/s02.jpg" alt="">
          <img src="img/sklad/s03.jpg" alt="">
          <img src="img/sklad/s04.jpg" alt="">
          <img src="img/sklad/s05.jpg" alt="">
          <img src="img/sklad/s06.jpg" alt="">
          <img src="img/sklad/s07.jpg" alt="">
        </div>
      </div>
      <div class="scr-sklad__col2">
        <form class="scr-sklad__form__wrapper"  action="sendmessage.php" method="POST">
          <div class="scr-sklad__form__title">Получите фото вашего обвеса</div>
          <div class="scr-sklad__form__subtitle">Выберите удобный способ связи:</div>
          <div class="scr-sklad__radio-wrapper">
            <input id="whatsapp" type="checkbox" class="scr-sklad__radio"  name="whatsapp" value="whatsapp">
            <label for="whatsapp"></label>

            <input id="viber" type="checkbox" class="scr-sklad__radio"  name="viber" value="viber">
            <label for="viber"></label>
          </div>
          <div class="scr-sklad__form__input-wrapper">
            <input type="text" class="scr-sklad__form__input" placeholder="Введите телефон" name="phone">
          </div>
          <div class="scr-sklad__form__input-wrapper">
            <input type="text" class="scr-sklad__form__input" placeholder="E-mail" name="email">
          </div>
          <input type="hidden" name="order" value="Получить фото товара">
          <input type="hidden" name="tagmanager" value="/all-parts-in-stock.html">
          <div class="scr-sklad__form__input-wrapper">
            <button class="scr-sklad__form__button" type="submit" data-content="Получить фото"></button>
          </div>
        </form>
      </div>
    </div>
  </section>

  <section class="scr-cons__wrapper wrapper">
    <div class="scr-cons__inner inner">
      <div class="scr-cons__title">Проконсультируем бесплатно</div>
      <form  action="sendmessage.php" method="POST">
        <div class="scr-cons__input-wrapper">
          <input type="text" class="scr-cons__input" placeholder="Введите телефон" name="phone">
        </div>
        <input type="hidden" name="order" value="Проконсультируем бесплатно">
        <input type="hidden" name="tagmanager" value="/consult-free.html">
        <div class="scr-cons__input-wrapper">
          <button class="scr-cons__button" type="submit" data-content="Получить консультацию"></button>
        </div>
      </form>
    </div>
  </section>


<section class="scr-sto__wrapper wrapper">
  <div class="scr-sto__inner inner">
    <div class="scr-sto__title">
      Сертифицированное СТО "Motul" в Харькове
    </div>
    <div class="scr-sto__subtitle">
      Полный спектр услуг по установке и сервису<br>
      <p style="color: #000;">Рынок ЛОСК, пл. Кононенко 1а</p>
    </div>
    <div class="slider_sto">
      <ul class="flip-items">
        <li>
          <img src="img/STO/1.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/2.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/3.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/4.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/sto.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/5.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/6.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/7.jpg" alt="">
        </li>
      </ul>
    </div>
  </div>
</section>

  <section class="scr-deliv__wrapper wrapper">
    <div class="scr-deliv__inner inner">
      <div class="scr-deliv__title">Доставляем по всей Украине</div>
      <img src="img/map.png" class="scr-deliv__img" alt="map">
      <form  action="sendmessage.php" method="POST" class="scr-deliv__form">
        <div class="scr-deliv__form-title">
          Закажите сейчас и уже<br>
          через 2 дня получите посылку!
        </div>
        <div class="scr-deliv__form-subtitle">
          Оплата при получении
        </div>
        <div class="scr-deliv__form-input-wrapper">
          <input type="text" class="scr-deliv__form-input" placeholder="Введите телефон" name="phone">
        </div>
        <input type="hidden" name="order" value="Доставляем по всей Украине">
        <input type="hidden" name="tagmanager" value="/delivery-all-ukr.html">
        <div class="scr-deliv__form-input-wrapper">
          <button class="scr-deliv__form-button" type="submit" data-content="Заказать"></button>
        </div>
      </form>
    </div>
  </section>

<section class="scr-logo__wrapper wrapper">
  <div class="scr-logo__inner inner">
    <div class="scr-logo__title">Получите фото обвеса для вашего автомобиля</div>
    <div class="scr-logo__subtitle">Выберите марку авто</div>
    <div class="scr-logo__logosBox">
      <div class="scr-logo__slide-item">
        <div class="scr-logo__slide-box" >
          <div class="hex-row">
            <div data-car="Audi" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/audi.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="BMW" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/bmw.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Caddilac" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/Caddy.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Chery" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/chery.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Chevrolet" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/chevrolet.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Chrysler" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/Chrysler.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Citroen" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/citro.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
          </div>
          <div class="hex-row even">
            <div data-car="Daewoo" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/daewoo.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Daihatsu" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/Daihatsu.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Dodge" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/Dodge.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Ford" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/ford.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Gaz" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/GAZ_Logo.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Geely" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/Geely_Logo.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
          </div>
          <div class="hex-row">
            <div data-car="Great_Wall" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/Great_Wall.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Honda" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/honda.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Hummer" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/Hummer_logo.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Hyundai" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/hyindai.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Infinity" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/infinity.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Jeep" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/jeep.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Kia" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/kia.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="scr-logo__slide-item">
        <div class="scr-logo__slide-box">
          <div class="hex-row">
            <div data-car="Lada" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/Lada.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Land rover" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/landrover.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Lexus" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/lexus.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Lifan" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/lifan.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Mazda" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/mazda.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Mersedes" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/mersedes.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Mitsubishi" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/mitsub.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
          </div>
          <div class="hex-row even">
            <div data-car="Nissan" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/nissan.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Opel" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/opel.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Peugeot" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/pegeot.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Porsche" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/porshe.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Renault" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/renaut.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Skoda" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/skoda.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
          </div>
          <div class="hex-row">
            <div data-car="SsangYong" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/SsangYong.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Subaru" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/subary.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Suzuki" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/suzuki-car-logo-png-hd-sk.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Toyota" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/toyota.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="UAZ" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/uaz-logo.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Volvo" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/volvo.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
            <div data-car="Volkswagen" class="hex">
              <div class="top"></div>
              <div class="middle">
                <img class="logocar" src="img/logo/vw.png" alt="">
              </div>
              <div class="bottom"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="scr-logo__form">
      <form action="sendmessage.php" method="POST">
        <div class="scr-logo__chbox-wrapper">
          <input id="checkbox1" type="checkbox" class="scr-logo__checkbox"  name="product1" value="Защита переднего бампера">
          <label for="checkbox1">Защита переднего бампера</label>

          <input id="checkbox2" type="checkbox" class="scr-logo__checkbox"  name="product2" value="Защита заднего бампера">
          <label for="checkbox2">Защита заднего бампера</label>

          <input id="checkbox3" type="checkbox" class="scr-logo__checkbox"  name="product3" value="Боковая защита бампера">
          <label for="checkbox3">Боковая защита бампера</label>

          <input id="checkbox4" type="checkbox" class="scr-logo__checkbox"  name="product4" value="Рейлинги">
          <label for="checkbox4">Рейлинги</label>
        </div>

        <div class="scr-logo__input-wrapper">
          <label for="carInput">Ваш автомобиль</label>
          <input id="carInput" type="text" class="scr-logo__input" placeholder="марка" name="auto">
        </div>
        <div class="scr-logo__input-wrapper">
          <label for="carInputModel">Модель</label>
          <input id="carInputModel" type="text" class="scr-logo__input" placeholder="модель" name="car_model">
        </div>
        <div class="scr-logo__input-wrapper">
          <label for="carYear">Год выпуска</label>
          <input id="carYear" type="text" class="scr-logo__input" name="car_year">
        </div>
        <div class="scr-logo__input-wrapper">
          <label for="phoneInput">Введите телефон</label>
          <input id="phoneInput" type="text" class="scr-logo__input" placeholder="+38___-___-__-__" name="phone">
        </div>
        <input type="hidden" name="order" value="Интерактивная форма с логотипами">
        <input type="hidden" name="tagmanager" value="/form-with-logos.html">
        <div class="scr-logo__input-wrapper">
          <div class="scr-logo__back" data-content="Назад"></div>
          <button class="scr-logo__button" type="submit" data-content="Подобрать"></button>
        </div>
      </form>
    </div>
  </div>

</section>


  <section class="scr-question__wrapper wrapper">
    <div class="scr-question__inner inner">
      <div class="scr-question__title">Появились вопросы?</div>
      <form  action="sendmessage.php" method="POST" class="scr-question__form">
        <div class="scr-question__form-title">
          Ответим на все вопросы
        </div>
        <div class="scr-question__form-subtitle">
          Перезвоним в течении 5 минут
        </div>
        <div class="scr-question__form-input-wrapper">
          <textarea cols="33" rows="10" class="scr-question__form-textarea" placeholder="Ваш вопрос" name="question"></textarea>
        </div>
        <div class="scr-question__form-input-wrapper">
          <input type="text" class="scr-question__form-input" placeholder="Введите телефон" name="phone">
        </div>
        <input type="hidden" name="order" value="Последний экран">
        <input type="hidden" name="tagmanager" value="/last-form.html">
        <div class="scr-question__form-input-wrapper">
          <button class="scr-question__form-button" type="submit" data-content="Задать вопрос"></button>
        </div>
      </form>
      <div class="scr-question__link"><img src="img/kung.png" alt=""><a href="/kungs ">Наш сайт с кунгами для пикапов. </a></div>
    </div>
  </section>







  <div class="modal callback-modal" id="callback-modal" data-type="modal">
    <div class="modal__wrapper modal-wrapper">
      <span class="modal__close modal-close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        ЗАКАЗАТЬ<br>
        БЕСПЛАТНЫЙ<br>
        ЗВОНОК
      </span>
        <form action="sendmessage.php" method="POST">
          <div class="modal__input-wrapper">
            <label class="modal__label" for="callback-modal-text-1">Введите номер телефона</label>
            <input type="text" id="callback-modal-text-1" class="modal__input" name="phone" placeholder="+38 (___) ___-__-__">
          </div>
          <input type="hidden" name="order" value="">
          <div class="modal__input-wrapper">
            <button type="submit" class="modal__button" data-content="Отправить"></button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal response-modal" data-type="modal" id="response-modal">
    <div class="modal__wrapper">
      <span class="modal__close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        Сообщение отправлено!<br>
        Мы свяжемся с Вами в ближайшее время.
      </span>
      </div>
    </div>
  </div>

  <script src="js/common.js"></script>
</body>

</html>
