(function(){
  var crEl = document.createElement.bind(document);
  $.fn.livesearch = function(arr){
    function search(str){
      var res = [];
      if(!str) return res;
      for (var i = 0; i < arr.length; i++) {
        // debugger;
        var el = arr[i].toLowerCase();
        if (el.indexOf(str.toLowerCase())!==-1) res.push(i);
        if (res.length>5) break;
      }
      return res;
    }
    function buildResNodes(arr){
      var nodes = []
      for (var i = 0; i < arr.length; i++) {
        var el = arr[i];
        var div = crEl('div');
        div.innerHTML = el;
        div.className = 'live-search__result';
        nodes.push(div);
      };
      return nodes;
    }
    function clear(val) {
      $(wrap).css({
        display:'none',
      });
      if (val) self.val(val);
    }
    var timerid;
    var self = this;
    var top = this.offset().top + this.height();
    var left = this.offset().left;
    var width = this.outerWidth();
    var wrap = crEl('div');
    $(wrap).addClass('live-search');
    $(wrap).css({
      width: width+'px'
    })
    this.after(wrap);
    var index = -1;
    this.on('keyup', function(e){
      var keyCode = e.keyCode; //38 - вверх; 40 - вниз
      if (keyCode == 40 || keyCode == 38) {
        var children = $(wrap).children();
        var maxIndex = children.length;
        $(children).removeClass('focus');
        if (maxIndex) {
          if (keyCode==40) {
            var child = (index < maxIndex -1) ?  children[++index] : children[maxIndex-1];
          } else {
            var child = (index >= 1) ?  children[--index] : children[0];
          }
        }
        this.value = child.textContent;
        $(child).addClass('focus');
        return;
      }
      if (keyCode == 27 || keyCode == 13) {
        clear();
        return ;
      }
      index = -1;
      var result = [];
      var query = this.value;
      if (!query) {
        clear();
        return ;
      }
      var res = search(query);
      if (!res.length) {
        clear();
        return ;
      }
      for (var i = 0; i < res.length; i++) {
        result.push(arr[res[i]])
      }
      var nodes = buildResNodes(result);
      wrap.innerHTML = '';
      for (var i = 0; i < nodes.length; i++) {
        $(wrap).append(nodes[i]);
      }
      $(wrap).css({
        display: 'block'
      });
    });
    $(wrap).on('click', function(e){
      if ($(e.target).hasClass('live-search__result')) clear(e.target.textContent);
    });
    this.closest('form').on('submit', function(){
      clear();
    });
    this.on('blur', function(){
      timerid = setTimeout(function(){
        clear();
      }, 100);
    });
  }
})(jQuery)
