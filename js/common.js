$(window).on('load', function () {
  var $preloader = $('#page-preloader'),
      $spinner   = $preloader.find('.spinner');
  $spinner.fadeOut();
  $preloader.delay(150).fadeOut('slow');
});
function Model() {
  this.events = {};
  this.callEvent = function(eventName, args){
    var eventArr = this.events[eventName];
    if (!!eventArr) {
      for (var i = 0; i < eventArr.length; i++) {
        eventArr[i].call(this, args ? args : undefined);
      }
    };
  };
  this.on = function(eventName, callback) {
    if (!this.events[eventName]) {
      this.events[eventName] = [];
    }
    this.events[eventName].push(callback.bind(this));
  };
  this.off = function(eventName) {
    if (!this.events[eventName]) return;
    this.events[eventName] = [];
  }
};
var events = new Model();

function unserializeUrl(){
  var pairs = window.location.search;
  if (window.location.search=='') return '';
  var temp;
  var obj = {};
  pairs = pairs.substring(1).split('&');
  for (var i = 0; i < pairs.length; i++) {
    temp = pairs[i].split('=');
    obj[temp[0]] = temp[1];
  }
  return obj;
}
$.validator.addMethod('uaphone', function(value, element, params){
  var regex = /\+38 \(0[0-9]{2}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/g;
  var match = value.match(regex);
  if (match!==null) {
    if (match.length === 1) return true;
  }
  return false;
});
$('input[name="phone"]').mask('+38 (999) 999-99-99');

$(document).ready(function() {
  $.ajax({
    url: '/js/models.json',
    dataType: 'json',
  })
  .done(function(resp) {
    events.callEvent('model-recived', resp);
  });
  events.on('model-recived', function(data){
    var inputs = $('[name="auto"]').not('[id="carInput"]').get();
    for (var i = 0; i < inputs.length; i++) {
      $(inputs[i]).attr('autocomplete','off').livesearch(data);
    }
  })
  var utm = unserializeUrl();
  var title = 'Качественный обвес из нержавейки<br>для надежной защиты вашего автомобиля';
  var titles = {
    'reilingi_poisk': 'Рейлинги из нержавейки',
    'peredniy_bamper_poisk': 'Защита переднего бампера<br>На складе 1024 единицы для 120 марок авто',
    'bokovaya_zaschita_poisk': 'Боковая защита<br>На складе 1024 единицы для 120 марок авто',
    'zadniy_bamper_poisk': 'Защита заднего бампера<br>На складе 1024 единицы для 120 марок авто',
    'vazy_poisk': 'Обвесы для вашего авто<br>На складе 1024 единицы для 120 марок авто',
    'farkop_poisk': 'Фаркопы для Вашего Авто<br>На складе 1024 единицы для 120 марок авто'
  };
  if (typeof utm === 'object') {
    if (titles[utm['utm_campaign']]) {
      title = titles[utm['utm_campaign']];
      var campaign = utm['utm_campaign'];
      var frontBlock = document.querySelector('#front');
      var needBlock = document.querySelector('[data-utm="'+campaign+'"]');
      $(needBlock).after(frontBlock);
      $('#after').after(needBlock);
      var triangle = document.querySelector('.scr-front__triangle');
      $(needBlock).prepend(triangle);
      // var beforeFront = $(frontBlock).next()[0];
      // var beforeNeed = $(needBlock).next()[0];
      // document.body.insertBefore(beforeFront, needBlock);
      // document.body.insertBefore(beforeNeed, frontBlock);
      // frontBlock.remove();
    }
  };
  $('#utm-title').html(title);
  $.ajax({
    url: "api.php"
  })
  .done(function (data) {
    var obj = JSON.parse(data);
    events.callEvent('currencyLoaded', {currency: +obj[2].sale});
  });
  events.on('currencyLoaded', function(data){
    var cur = data.currency;
    $('li[data-price]').each(function(){
      var uaprice = $(this).attr('data-price');
      var lol = uaprice*cur;
      newuaprice = $(this).append('<div class="dollar">от '+lol+' грн.</div>');
    });
  });

  var forms = document.getElementsByTagName('form');
  for (var i = 0; i < forms.length; i++) {
    $(forms[i]).validate({
      rules: {
        phone: {
          required: true,
          uaphone: true,
        },
      },
      messages: {
        phone: {
          required: 'Введите номер телефона',
          uaphone: 'Введите корректный номер телефона'
        }
      },
      submitHandler: submitForm
    });
  };
  $('[data-init="modal"]').on('click', function() {
    var selector = $(this).attr('data-modal');
    var order = $(this).attr('data-order');
    var hiddenValue = $(this).attr('data-hidden');
    var modal = document.querySelector(selector);
    var form = modal.querySelector('form');
    var removed = form.querySelector('input[name="tagmanager"]');
    if (removed) removed.remove();
    var hidden = document.createElement('input');
    hidden.setAttribute('type', 'hidden');
    hidden.setAttribute('name', 'tagmanager');
    hidden.value = hiddenValue;
    form.appendChild(hidden);
    $(modal).addClass('active').find('[name="order"]').val(order);
  });
  $('.modal-close').on('click', function() {
    $(this).parent().parent().removeClass('active');
  });
  $('[data-type="modal"]').on('click', function() {
    $(this).removeClass('active');
  });
  $('.modal-wrapper').on('click', function(event) {
    event.stopPropagation();
  });
  $('.fancybox').fancybox({
    padding:0,
  });
  function submitForm(form, e){
    var data = $(form).serialize();
    var text = $(form).find('button').attr('data-content');
    var page = $(form).find('[name="tagmanager"]').val();
    $.ajax({
          url: 'sendmessage.php',
          type: 'POST',
          data: data,
          beforeSend: function(){
            $(form).find('input, button').attr('disabled', '');
            $(form).find('button').attr('data-content', 'Отправляем...');
          }
        })
        .done(function(response) {
          $(form).find('input, button').removeAttr('disabled');
          $(form).find('input[name="phone"]').val('');
          $(form).find('button').attr('data-content',text);
          $('[data-type="modal"]').removeClass('active');
          $('#response-modal').addClass('active');
          dataLayer.push({
            'event' : 'VirtualPageview',
            'virtualPageURL' : page,
            'virtualPageTitle' : page.replace('/', '')
          });
        })
        .fail(function(response) {
          console.log(response);
        });
  }


   $('.scr-logo__logosBox').slick({
     draggable:false
   });
  $('.slider_sklad').slick({
    draggable: true,
    autoplay:true,
    dots: true,
    fade: true,
    autoplaySpeed:1500
  });
//появление формы при нажатии на логотим авто
  $('.hex').click(function(){
    var car = $(this).attr('data-car');
    $('#carInput').val(car);
    $('.scr-logo__logosBox').hide();
    $('.scr-logo__subtitle').hide();
    $('.scr-logo__form').show();
  });
  $('.scr-logo__back').click(function(){
    $('.scr-logo__form').hide();
    $('.scr-logo__logosBox').show();
    $('.scr-logo__subtitle').show();
  });

//scroll-to
  $('#frontIcon').click(function(){
    var elementClick=($('#front').offset().top)+100;
    $('html,body').animate({scrollTop: elementClick},800);
    return false;
  });
  $('#sidewall').click(function(){
    var elementClick=$('#side').offset().top;
    $('html,body').animate({scrollTop: elementClick},800);
    return false;
  });
  $('#rear').click(function(){
    var elementClick=$('#back').offset().top;
    $('html,body').animate({scrollTop: elementClick},800);
    return false;
  });
  $('#Roof').click(function(){
    var elementClick=$('#rails').offset().top;
    $('html,body').animate({scrollTop: elementClick},800);
    return false;
  });
  $('#korch').click(function(){
    var elementClick=$('#black').offset().top;
    $('html,body').animate({scrollTop: elementClick},800);
    return false;
  });

  $('.look').click(function(){
    var elementClick=($(this).offset().top)+550-($(window).height());
    $('html,body').animate({scrollTop: elementClick},600);
    return false;
  });

  $("img.lazy").lazyload({
    effect : "fadeIn"
  });

  var slider_sto = $(".slider_sto").flipster({
    style: 'flat',
    spacing: -0.25,
    nav: false,
    buttons:   true,
    scrollwheel: false,
    loop: true
  });
});
