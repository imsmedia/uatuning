module.exports = function(grunt) {
  var mozjpeg = require('imagemin-mozjpeg');
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    imagemin: {                          // Task
      dynamic: {
        options: {                       // Target options
          optimizationLevel: 4,
          use: [mozjpeg()]
        },
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: 'img/',                   // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif,JPG,jpeg}'],   // Actual patterns to match
          dest: 'img-build/'                  // Destination path prefix
        }]
      }
    },
    watch: {
      all: {
        files: ['js/*.js', '*.php'],
        options: {
          livereload: true,
        },
      },
      css: {
        files: ['scss/*.scss'],
        tasks: ['sass'],
        options: {
          spawn: false,
          livereload: true,
        },
      },
    },
    sass: {
      dist: {
        options: {
          compass: true,
          style: 'compressed'
        },
        files: {
          'css/style.css': 'scss/style.scss',
        }
      }
    },
  });
  require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.registerTask('default', ['watch']);
};
