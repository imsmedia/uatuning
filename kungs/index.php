<?php session_start();
$site_url = 'http://ua-tuning.com/kungs/';
if (array_key_exists('utm_referrer', $_GET)) {
  if (strpos($site_url, $_GET['utm_referrer'])===false) {
    $_SESSION['referer'] = $_GET['utm_referrer'];
  }
}
if (array_key_exists('utm_source', $_GET)) {
  if (strpos($site_url, $_GET['utm_source'])===false) {
    $_SESSION['sourse'] = $_GET['utm_source'];
  }
  if (strpos($site_url, $_GET['utm_term'])===false) {
    $_SESSION['term'] = $_GET['utm_term'];
  }
  if (strpos($site_url, $_GET['utm_campaign'])===false) {
    $_SESSION['campaign'] = $_GET['utm_campaign'];
  }
}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <title>UA-Tuning Kungs</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=1024">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <!--<link rel="stylesheet" href="js/libs/jquery-ui/jquery-ui.min.css">-->
  <link rel="stylesheet" href="js/libs/flipster/jquery.flipster.css" media="screen" title="no title" charset="utf-8">
  <!--<link rel="stylesheet" href="css/owl.carousel.css" media="screen" title="no title" charset="utf-8">-->
  <link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
  <script src="js/libs/jquery.min.js"></script>
  <!--<script src="js/libs/jquery-ui/jquery-ui.min.js"></script>-->
  <script src="js/libs/maskedinput.min.js"></script>
  <script src="js/libs/validate/jquery.validate.min.js"></script>
  <script src="js/libs/fancybox/jquery.fancybox.pack.js"></script>
  <script src="js/tabs.js"></script>
  <!--<script src="js/libs/owl.carousel.js"></script>-->
  <script src="js/libs/slick.min.js"></script>
  <script src="js/libs/flipster/jquery.flipster.min.js"></script>
  <script src="/js/livesearch.js" charset="utf-8"></script>
  <!--<script src="js/libs/jquery.lazyload.min.js"></script>-->
</head>

<body>
<!-- Google Tag Manager -->
<script>dataLayer=[];</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MQZGM3"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MQZGM3');</script>
<!-- End Google Tag Manager -->
<div id="page-preloader">
  <div class="body">
  	<span>
      	<span></span>
      	<span></span>
      	<span></span>
      	<span></span>
    </span>
    <div class="base">
      <span></span>
      <div class="face"></div>
    </div>
  </div>
  <div class="longfazers">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <h1>Секундочку...</h1>
</div>

<header class="inner header">
  <img class="header__logo" src="img/logo.png" alt="El Auto">
  <div class="header__buttonbox" tabindex="1">
    <div class="header__buttonbox__button" data-hidden="Хедер заказать звонок" data-content="Заказать звонок" data-init="modal" data-modal="#callback-modal"  data-order="Заказ обратного звонка (шапка сайта)" data-hidden="/callback-header.html"></div>
  </div>
      <span class="header__phone">
        <a class="header__link" href="tel:++380503232740">+38(050) 323-27-40</a>
              Работаем без выходных
      </span>
</header>

<section class="wrapper scr-i__wrapper" id="back">
  <div class="inner scr-i">
      <span class="scr-i__title" id="title">
        Обеспечьте безопасность багажа с надежными
      </span>
      <p class="scr-i__subtitle">
        кунгами и багажниками
      </p>
    <div class="scr-i__fiches-wrapper">
      <div class="scr-i__fiches-wrapper-item">
        Дополнительное пространство в кузове
      </div>
      <div class="scr-i__fiches-wrapper-item">
        Защита груза от попадания влаги
      </div>
      <div class="scr-i__fiches-wrapper-item">
        Элегантный<br>внешний вид
      </div>
    </div>
    <div class="scr-i__form-wrapper">
<!--        <span class="scr-i__form-title">-->
<!--          Подберите аксессуары для своего пикапа-->
<!--        </span>-->
<!--        <span class="scr-i__form-subtitle">-->
<!--          и закажи со скидкой 10%-->
<!--        </span>-->
        <span class="scr-i__form-subtitle">
          Получите варианты аксессуаров<br>для вашего пикапа
        </span>
      <div class="scr-i__form">
        <form action="sendmessage.php" id="form-1" method="POST">
          <div class="scr-i__input-wrapper">
            <input type="text" class="scr-i__input" placeholder="Введите марку авто" name="auto">
          </div>
          <div class="scr-i__input-wrapper--z1">
            <input type="text" class="scr-i__input" placeholder="Введите телефон" name="phone">
          </div>
          <input type="hidden" name="order" value="Первый экран-форма">
          <input type="hidden" name="tagmanager" value="/first-screen-form.html">
          <div class="scr-i__input-wrapper--z1">
            <button class="scr-i__button" type="submit" data-content="Получить"></button>
          </div>
        </form>
      </div>
      <span class="scr-i__form-descr">
          Доставка по всей Украине
        </span>
    </div>
  </div>
</section>



<section class="scr-iii__wrapper wrapper">
  <div class="scr-iii__inner inner">
      <span class="scr-iii__title">
        Кунги и багажники для пикапов в наличии
      </span>
    <div id="tabs" class="tabs">
      <div class="tabs-nav">
        <ul>
          <li class="tabs-nav__item" data-content="Кунги Canopy"></li>
          <li class="tabs-nav__item" data-content="Full Box"></li>
          <li class="tabs-nav__item" data-content="Grand Box"></li>
          <li class="tabs-nav__item" data-content="Star Box"></li>
          <li class="tabs-nav__item" data-content="Ролеты"></li>
        </ul>
      </div>
      <div class="tabs-content">
        <ul>
          <li class="tabs-content__item tab">
            <div class="carousel">
              <ul class="flip-items">
                <li data-flip-title="Hilux 15+" data-flip-category="Toyota">
                  <img src="img/Product/Canopy/Toyota-hulix-15+/23_1.jpg" alt="">
                </li>
                <li data-flip-title="Hilux 07-15" data-flip-category="Toyota">
                  <img src="img/Product/Canopy/Toyota-hulix-(07-15)/21_1.jpg" alt="">
                </li>
                <li data-flip-title="Ranger T5" data-flip-category="Ford">
                  <img src="img/Product/Canopy/Ford-ranger-t5/1_1.jpg" alt="">
                </li>
                <li data-flip-title="Ranger T6" data-flip-category="Ford">
                  <img src="img/Product/Canopy/Ford-ranger-t6/3_1.jpg" alt="">
                </li>
                <li data-flip-title="L 200" data-flip-category="Mitsubishi">
                  <img src="img/Product/Canopy/l200-(shot,long)/7_1.jpg" alt="">
                </li>
                <li data-flip-title="L 200new" data-flip-category="Mitsubishi">
                  <img src="img/Product/Canopy/new-l200/10_1.jpg" alt="">
                </li>
                <li data-flip-title="NP-300" data-flip-category="Nissan">
                  <img src="img/Product/Canopy/Nissan-np-300/14_1.jpg" alt="">
                </li>
                <li data-flip-title="NP-300(2015)" data-flip-category="Nissan">
                  <img src="img/Product/Canopy/Nissan-np-300-15+/16_1.jpg" alt="">
                </li>
                <li data-flip-title="Navara" data-flip-category="Nissan">
                  <img src="img/Product/Canopy/Nissan-Navara/12_1.jpg" alt="">
                </li>
                <li data-flip-title="Wingle" data-flip-category="Great_Wall">
                  <img src="img/Product/Canopy/Great-Wall-wingle-5/6_1.jpg" alt="">
                </li>
                <li data-flip-title="Action sport 07-12" data-flip-category="Ssang_Yong">
                  <img src="img/Product/Canopy/Ssang-Yong-action-sport-07-12/17_1.jpg" alt="">
                </li>
                <li data-flip-title="Action sport 12+" data-flip-category="Ssang_Yong">
                  <img src="img/Product/Canopy/Ssang-Yong-action-sport-12+/19_1.jpg" alt="">
                </li>
                <li data-flip-title="Amarok" data-flip-category="VW">
                  <img src="img/Product/Canopy/VW-Amarok/26_1.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Материал:</strong> Стекловолокно</p>
                <p><strong>Цвет:</strong>Под покраску</p>
                <p><strong>Страна:</strong>Турция</p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="1200"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal"  data-order="Купить по акции Canopy" data-hidden="/buy-with-sale.html"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 8 акционных товаров</div>
              </div>
            </div>
          </li>
          <li class="tabs-content__item tab">
            <div class="carousel">
              <ul class="flip-items">
                <li data-flip-title="NP-300" data-flip-category="Nissan">
                  <img src="img/Product/Full-Box/Nissan-np-300/37_1.jpg" alt="">
                </li>
                <li data-flip-title="Navara" data-flip-category="Nissan">
                  <img src="img/Product/Full-Box/Nissan-Navara/36_1.jpg" alt="">
                </li>
                <li data-flip-title="Ranger T5" data-flip-category="Ford">
                  <img src="img/Product/Full-Box/Ford-ranger-t5/27_1.jpg" alt="">
                </li>
                <li data-flip-title="Ranger T6" data-flip-category="Ford">
                  <img src="img/Product/Full-Box/Ford-ranger-t6/30_1.jpg" alt="">
                </li>
                <li data-flip-title="L 200" data-flip-category="Mitsubishi">
                  <img src="img/Product/Full-Box/l200-(shot,long)/31_1.jpg" alt="">
                </li>
                <li data-flip-title="L 200new" data-flip-category="Mitsubishi">
                  <img src="img/Product/Full-Box/new-l200/33_1.jpg" alt="">
                </li>
                <li data-flip-title="Action sport 07-12" data-flip-category="Ssang_Yong">
                  <img src="img/Product/Full-Box/SsangYong-action-sport%2007-12/40_1.jpg" alt="">
                </li>
                <li data-flip-title="Hilux 07-15" data-flip-category="Toyota">
                  <img src="img/Product/Full-Box/Toyota-hulix-(07-15)/41_1.jpg" alt="">
                </li>
                <li data-flip-title="Hilux 15+" data-flip-category="Toyota">
                  <img src="img/Product/Full-Box/Toyota-hulix-15+/43_1.jpg" alt="">
                </li>
                <li data-flip-title="Amarok" data-flip-category="VW">
                  <img src="img/Product/Full-Box/VW-Amarok/44_1.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Материал:</strong> Стекловолокно</p>
                <p><strong>Цвет:</strong>Под покраску</p>
                <p><strong>Страна:</strong>Турция</p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="1150"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal"  data-hidden="/buy-with-sale.html" data-order="Купить по акции Full-Box"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 8 акционных товаров</div>
              </div>
            </div>
          </li>
          <li class="tabs-content__item tab">
            <div class="carousel">
              <ul class="flip-items">
                <li data-flip-title="Ranger T6" data-flip-category="Ford">
                  <img src="img/Product/Grand-box/Ford-ranger-t6/47_1.jpg" alt="">
                </li>
                <li data-flip-title="NP-300(2015)" data-flip-category="Nissan">
                  <img src="img/Product/Grand-box/Nissan-np-300-15+/51_1.jpg" alt="">
                </li>
                <li data-flip-title="Navara" data-flip-category="Nissan">
                  <img src="img/Product/Grand-box/Nissan-Navara/49_1.jpg" alt="">
                </li>
                <li data-flip-title="Hilux 07-15" data-flip-category="Toyota">
                  <img src="img/Product/Grand-box/Toyota-hulix-(07-15)/53_1.jpg" alt="">
                </li>
                <li data-flip-title="Hilux 15+" data-flip-category="Toyota">
                  <img src="img/Product/Grand-box/Toyota%20hulix%2015+/54_1.jpg" alt="">
                </li>
                <li data-flip-title="Amarok" data-flip-category="VW">
                  <img src="img/Product/Grand-box/VW-Amarok/55_1.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Материал:</strong> Стекловолокно</p>
                <p><strong>Цвет:</strong>Под покраску</p>
                <p><strong>Страна:</strong>Турция</p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="1200"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal" data-hidden="/buy-with-sale.html" data-order="Купить по акции Grand-box"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 8 акционных товаров</div>
              </div>
            </div>
          </li>
          <li class="tabs-content__item tab">
            <div class="carousel">
              <ul class="flip-items">
                <li data-flip-title="Navara" data-flip-category="Nissan">
                  <img src="img/Product/Star-box/Nissan-Navara/68_1.jpg" alt="">
                </li>
                <li data-flip-title="NP-300(2015)" data-flip-category="Nissan">
                  <img src="img/Product/Star-box/Nissan-NP-300-7-15/70_1.jpg" alt="">
                </li>
                <li data-flip-title="Ranger T5" data-flip-category="Ford">
                  <img src="img/Product/Star-box/Ford-Ranger-T5/64_1.jpg" alt="">
                </li>
                <li data-flip-title="L 200" data-flip-category="Mitsubishi">
                  <img src="img/Product/Star-box/l200-(shot,long)/66_1.jpg" alt="">
                </li>
                <li data-flip-title="Hilux 07-15" data-flip-category="Toyota">
                  <img src="img/Product/Star-box/Toyota-hulix-(07-15)/72_1.jpg" alt="">
                </li>
                <li data-flip-title="Amarok" data-flip-category="VW">
                  <img src="img/Product/Star-box/VW-Amarok/75_1.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Материал:</strong> Стекловолокно</p>
                <p><strong>Цвет:</strong>Под покраску</p>
                <p><strong>Страна:</strong>Турция</p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="1200"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal" data-hidden="/buy-with-sale.html" data-order="Купить по акции Star-box"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 8 акционных товаров</div>
              </div>
            </div>
          </li>
          <li class="tabs-content__item tab">
            <div class="carousel">
              <ul class="flip-items">
                <li data-flip-title="Navara" data-flip-category="Nissan">
                  <img src="img/Product/Rollets/Nissan-Navara/59_1.jpg" alt="">
                </li>
                <li data-flip-title="L 200" data-flip-category="Mitsubishi">
                  <img src="img/Product/Rollets/l200-(shot,long)/57_1.jpg" alt="">
                </li>
                <li data-flip-title="L 200 new" data-flip-category="Mitsubishi">
                  <img src="img/Product/Rollets/new-l200/58_1.jpg" alt="">
                </li>
                <li data-flip-title="Action sport 07-12" data-flip-category="Ssang_Yong">
                  <img src="img/Product/Rollets/Ssang-Yong-action-sport-07-12/60_1.jpg" alt="">
                </li>
                <li data-flip-title="Hilux 15+" data-flip-category="Toyota">
                  <img src="img/Product/Rollets/Toyota-hulix-15+/62_1.jpg" alt="">
                </li>
                <li data-flip-title="Amarok" data-flip-category="VW">
                  <img src="img/Product/Rollets/VW-Amarok/63_1.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Материал:</strong> Стекловолокно</p>
                <p><strong>Цвет:</strong>Под покраску</p>
                <p><strong>Страна:</strong>Турция</p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="1000"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal" data-hidden="/buy-with-sale.html" data-order="Купить по акции Rollets"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 8 акционных товаров</div>
              </div>
            </div>
          </li>

        </ul>
      </div>
    </div>
  </div>
</section>

<section class="scr-iii__wrapper wrapper">
  <div class="scr-iii__inner inner">
      <span class="scr-iii__title">
        Аксессуары и запчасти
      </span>
    <div id="tabs2" class="tabs">
      <div class="tabs-nav">
        <ul>
          <li class="tabs-nav__item" data-content="Корыто в кузов"></li>
          <li class="tabs-nav__item" data-content="Рейлинги и перемычки для кунгов"></li>
          <li class="tabs-nav__item" data-content="Roll-bar"></li>
          <li class="tabs-nav__item" data-content="Запчасти для кунгов"></li>
        </ul>
      </div>
      <div class="tabs-content">
        <ul>
          <li class="tabs-content__item tab">
            <div class="carousel2">
              <ul class="flip-items">
                <li>
                  <img src="img/Product/Korito/01.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Korito/kk1.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Korito/kk2.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Korito/04.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Комплектующие и<br> аксессуары для пикапов</strong></p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="225"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal" data-hidden="/buy-with-sale.html" data-order="Купить по акции корыто"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 8 акционных товаров</div>
              </div>
            </div>
          </li>
          <li class="tabs-content__item tab">
            <div class="carousel2">
              <ul class="flip-items">
                <li>
                  <img src="img/Product/Raylingi/06.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Raylingi/+rk1.JPG" alt="">
                </li>
                <li>
                  <img src="img/Product/Raylingi/+rk2.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Raylingi/+rk3.JPG" alt="">
                </li>
                <li>
                  <img src="img/Product/Raylingi/+rk4.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Комплектующие и<br>аксессуары для пикапов</strong></p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="175"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal" data-hidden="/buy-with-sale.html" data-order="Купить по акции рейлинги"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 8 акционных товаров</div>
              </div>
            </div>
          </li>
          <li class="tabs-content__item tab">
            <div class="carousel2">
              <ul class="flip-items">
                <li>
                  <img src="img/Product/Roll-bar/07.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Roll-bar/08.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Roll-bar/09.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Roll-bar/+roll1.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Roll-bar/+roll2.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Roll-bar/+roll3.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Roll-bar/+roll4.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Комплектующие и<br> аксессуары для пикапов</strong></p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="275"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal" data-hidden="/buy-with-sale.html" data-order="Купить по акции Roll-bar"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 8 акционных товаров</div>
              </div>
            </div>
          </li>
          <li class="tabs-content__item tab">
            <div class="carousel2">
              <ul class="flip-items">
                <li>
                  <img src="img/Product/Zapchasti/001.JPG" alt="">
                </li>
                <li>
                  <img src="img/Product/Zapchasti/002.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Zapchasti/003.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Zapchasti/as1.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Zapchasti/as2.jpg" alt="">
                </li>
                <li>
                  <img src="img/Product/Zapchasti/as3.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="descr-box">
              <div class="descr-box__delineation">
                <p><strong>Комплектующие и<br> аксессуары для пикапов</strong></p>
                <p><strong>Срок доставки:</strong>1-3 дня</p>
              </div>
              <div class="descr-box__old-price"></div>
              <div class="descr-box__new-price" data-price="60"><p></p></div>
              <div class="descr-box__buy">
                <div class="descr-box__green_button" data-init="modal" data-modal="#callback-modal" data-hidden="/buy-with-sale.html" data-order="Купить по акции запчасти"><p>Купить по акции</p>
                </div>
                <div class="descr-box__last_one">Осталось 48 акционных товаров</div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="scr-advantages__wrapper wrapper">
  <div class="scr-advantages__inner inner">
    <div class="scr-advantages__title">
      Преимущества кунгов из стекловолокна
    </div>
    <div class="scr-advantages__col">
      <div class="scr-advantages__item">
        <div class="scr-advantages__item--title" style="background: #000">
          Высокая прочность
        </div>
        <div class="scr-advantages__item--descr">
          Многослойная структура  обеспечивает высокое сопротивление ударам и деформациям
        </div>
      </div>
      <div class="scr-advantages__item">
        <div class="scr-advantages__item--title">
          Не образует конденсат
        </div>
        <div class="scr-advantages__item--descr">
          В отличии от металла, стекловолокно имеет низкую теплопроводность, что исключает образование конденсата - ваш багаж всегда сух
        </div>
      </div>
      <div class="scr-advantages__item">
        <div class="scr-advantages__item--title" style="background: #000">
          Термоустойчивость
        </div>
        <div class="scr-advantages__item--descr">
          Низкий коэффициент термического расширения обеспечивает структурную целостность кунгов при перепадах температур
        </div>
      </div>
    </div>
    <div class="scr-advantages__col">
      <img src="img/Fiches/1.jpg" alt="">
    </div>
    <div class="scr-advantages__col">
      <div class="scr-advantages__item">
        <div class="scr-advantages__item--title">
          Устойчивы к коррозии
        </div>
        <div class="scr-advantages__item--descr">
          Химически и биологически нейтрален, не гниет, не разлагается, не ржавеет,
          не окисляется
        </div>
      </div>
      <div class="scr-advantages__item">
        <div class="scr-advantages__item--title" style="background: #000">
          Ремонтопригоден
        </div>
        <div class="scr-advantages__item--descr">
          В случае повреждения, может быть легко восстановлен при помощи эпоксидной смолы
        </div>
      </div>
      <div class="scr-advantages__item">
        <div class="scr-advantages__item--title">
          Под покраску
        </div>
        <div class="scr-advantages__item--descr">
          Может быть окрашен в любой цвет, надежно удерживает краску, в окрашенном состоянии неотличим от металла
        </div>
      </div>
    </div>

    <form class="scr-advantages__form__wrapper"  action="sendmessage.php" method="POST">
      <div class="scr-advantages__form--title">Подберем кунг или багажник под<br> цвет кузова вашего пикапа</div>
      <div class="scr-advantages__form__input-wrapper">
        <input type="text" class="scr-advantages__form__input" placeholder="Введите телефон" name="phone">
      </div>
      <input type="hidden" name="order" value="Подберем кунг или багажник под цвет кузова">
      <input type="hidden" name="tagmanager" value="/part-in-color.html">
      <div class="scr-advantages__form__input-wrapper">
        <button class="scr-advantages__form__button" type="submit" data-content="Получить информацию"></button>
      </div>
    </form>
  </div>
</section>

<section class="scr-stages__wrapper wrapper">
  <div class="scr-stages__inner inner">
    <div class="scr-stages__title">
      Как выбрать и заказать
    </div>
    <div class="scr-stages__item">
      <div class="scr-stages__item--image1"></div>
      <div class="scr-stages__item--title">Оставьте номер телефона и<br> получите консультацию</div>
      <div class="scr-stages__item--subtitle"  data-init="modal" data-modal="#callback-modal" data-hidden="Получить консультацию">Получить консультацию</div>
    </div>
    <div class="scr-stages__item">
      <div class="scr-stages__item--image2"></div>
      <div class="scr-stages__item--title">Выберите цвет<br>
        и оформите заказ</div>
      <div class="scr-stages__item--subtitle" data-init="modal" data-modal="#callback-modal" data-hidden="Подобрать цвет">Подобрать цвет</div>
    </div>
    <div class="scr-stages__item">
      <div class="scr-stages__item--image3"></div>
      <div class="scr-stages__item--title">Получите заказ и<br>
        установите на пикап</div>
      <!--<div class="scr-stages__item&#45;&#45;subtitle">Посмотреть видео установки</div>-->
    </div>
  </div>
</section>

<section class="scr-sklad__wrapper wrapper">
  <div class="scr-sklad__inner inner">
    <div class="scr-sklad__title">
      Все кунги уже на нашем складе
    </div>
    <div class="scr-sklad__col">
      <div class="slider_sklad">
        <img src="img/sklad/sp1.jpg" alt="">
        <img src="img/sklad/sp2.jpg" alt="">
        <img src="img/sklad/sp3.jpg" alt="">
        <img src="img/sklad/sp4.jpg" alt="">
      </div>
    </div>
    <div class="scr-sklad__col2">
      <form class="scr-sklad__form__wrapper"  action="sendmessage.php" method="POST">
        <div class="scr-sklad__form__title">Получите фото вашего кунга</div>
        <div class="scr-sklad__form__subtitle">Выберите удобный способ связи:</div>
        <div class="scr-sklad__radio-wrapper">
          <input id="whatsapp" type="checkbox" class="scr-sklad__radio"  name="whatsapp" value="whatsapp">
          <label for="whatsapp"></label>

          <input id="viber" type="checkbox" class="scr-sklad__radio"  name="viber" value="viber">
          <label for="viber"></label>
        </div>
        <div class="scr-sklad__form__input-wrapper">
          <input type="text" class="scr-sklad__form__input" placeholder="Введите телефон" name="phone">
        </div>
        <div class="scr-sklad__form__input-wrapper">
          <input type="text" class="scr-sklad__form__input" placeholder="E-mail" name="email">
        </div>
        <input type="hidden" name="order" value="Получить фото товара">
        <input type="hidden" name="tagmanager" value="/all-parts-in-stock.html">
        <div class="scr-sklad__form__input-wrapper">
          <button class="scr-sklad__form__button" type="submit" data-content="Получить фото"></button>
        </div>
      </form>
    </div>
  </div>
</section>

<section class="scr-map__wrapper wrapper">
  <div class="scr-map__inner inner">
    <div class="scr-map__col">
      <img src="img/map.jpg" alt="">
    </div>
    <div class="scr-map__col2">
      <div class="scr-map__descr">Доставляем за 3 дня в любую точку Украины</div>
      <img src="img/newpost.png" alt="">
    </div>
  </div>
</section>

<section class="scr-package__wrapper wrapper">
  <div class="scr-package__inner inner">
    <div class="scr-package__title">
      Каждый товар надежно упакован
    </div>
    <div class="scr-package__subtitle">
      Вероятность повреждения при перевозке исключена
    </div>
    <img src="img/package1.jpg" class="scr-package__img">
    <img src="img/package2.jpg" class="scr-package__img">
    <img src="img/package3.jpg" class="scr-package__img">
    <div class="scr-package__title">
      Гарантия возврата и обмена
    </div>
    <div class="scr-package__item">
      <div class="scr-package__item--image1"></div>
      <div class="scr-package__item--title">Каждый заказ тщательно проверяется перед отправкой на наличие всех необходимых комплектующих и отсутствие дефектов</div>
    </div>
    <div class="scr-package__item">
      <div class="scr-package__item--image3"></div>
      <div class="scr-package__item--title">В течении 14 дней после получения заказа вы имеете право вернуть или обменять товар в случаях, если были обнаружены дефекты или поломки</div>
    </div>
    <div class="scr-package__item">
      <div class="scr-package__item--image2"></div>
      <div class="scr-package__item--title">Гарантийный срок, в течении которого компания обеспечивает ремонт купленных кунгов или багажников, составляет 1 год.</div>
    </div>
  </div>
</section>


<section class="scr-iii__wrapper wrapper">
  <div class="scr-iii__inner inner">
    <div class="scr-iii__title">Почему уже <span>330</span> владельцев пикапов<br>
      купили наши кунги и багажники в 2015 году?</div>
    <div class="scr-iii__col">
      <div class="slider1">
        <img src="img/Fiches/2.jpg" alt="">
        <img src="img/Fiches/3.JPG" alt="">
        <img src="img/Fiches/5.jpg" alt="">
        <img src="img/Fiches/6.jpg" alt="">
        <img src="img/Fiches/4.jpg" alt="">
      </div>
    </div>
    <div class="scr-iii__col">
      <div class="scr-iii__col--item item1 active">
        <p>Подходят для вашего пикапа</p>
        Кунги и багажники спроектированы специально для авто вашей модели, идеально прилегают к кузову без зазоров и щелей.
      </div>
      <div class="scr-iii__col--item item2">
        <p>Установка за 15 минут</p>
        Вы можете самостоятельно установить кунг или багажник, следуя приложенной инструкции.
      </div>
      <div class="scr-iii__col--item item3">
        <p>Надежные замки</p>
        Замки производства германии гарантируют надежную защиту вашему
      </div>
      <div class="scr-iii__col--item item4">
        <p>Прочные стекла</p>
        Не трескаются на ухабах, улучшают обзор из салона через зеркало
      </div>
      <div class="scr-iii__col--item item5">
        <p>Защита от попадания влаги</p>
        Надежные резиновые уплотнители не допускают проникновения влаги
      </div>
    </div>
  </div>
</section>

<section class="scr-takeInfo__wrapper wrapper">
  <div class="scr-takeInfo__inner inner">
    <div class="scr-takeInfo__title">Получите подробную информацию по гарантийному и сервисному обслуживанию продукции нашей компании</div>
    <form class="scr-takeInfo__form__wrapper"  action="sendmessage.php" method="POST">
      <div class="scr-takeInfo__form__input-wrapper">
        <input type="text" class="scr-takeInfo__form__input" placeholder="Введите телефон" name="phone">
      </div>
      <input type="hidden" name="order" value="Получить информацию о гарантийному и сервисному обслуживанию">
      <input type="hidden" name="tagmanager" value="/garanty-services-form.html">
      <div class="scr-takeInfo__form__input-wrapper">
        <button class="scr-takeInfo__form__button" type="submit" data-content="Получить информацию"></button>
      </div>
    </form>
  </div>
</section>

<!--<section class="scr-accessories__wrapper wrapper">-->
  <!--<div class="scr-accessories__inner inner">-->
    <!--<div class="scr-accessories__title">Аксессуары и запчасти</div>-->
    <!--<div class="scr-accessories__item">-->
      <!--<img src="img/korito.jpg" alt="">-->
      <!--<div class="scr-accessories__item&#45;&#45;title">-->
        <!--Корыто в кузов-->
      <!--</div>-->
      <!--<div class="scr-accessories__item&#45;&#45;price"><p>5 965грн</p></div>-->
      <!--<div class="scr-accessories__item&#45;&#45;green_button"  data-init="modal" data-modal="#callback-modal"><p>Подобрать</p></div>-->
    <!--</div>-->
    <!--<div class="scr-accessories__item">-->
      <!--<img src="img/reling.jpg" alt="">-->
      <!--<div class="scr-accessories__item&#45;&#45;title">-->
        <!--Рейлинги и перемычки<br>-->
        <!--для кунгов-->
      <!--</div>-->
      <!--<div class="scr-accessories__item&#45;&#45;price"><p>4 680грн</p></div>-->
      <!--<div class="scr-accessories__item&#45;&#45;green_button"  data-init="modal" data-modal="#callback-modal"><p>Подобрать</p></div>-->
    <!--</div>-->
    <!--<div class="scr-accessories__item">-->
      <!--<img src="img/parts.jpg" alt="">-->
      <!--<div class="scr-accessories__item&#45;&#45;title">-->
        <!--Запчасти для кунгов-->
      <!--</div>-->
      <!--<div class="scr-accessories__item&#45;&#45;price"><p>1000грн</p></div>-->
      <!--<div class="scr-accessories__item&#45;&#45;green_button"  data-init="modal" data-modal="#callback-modal"><p>Подобрать</p></div>-->
    <!--</div>-->
    <!--<div class="scr-accessories__item">-->
      <!--<img src="img/Roll.jpg" alt="">-->
      <!--<div class="scr-accessories__item&#45;&#45;title">-->
        <!--Roll-bar-->
      <!--</div>-->
      <!--<div class="scr-accessories__item&#45;&#45;price"><p>7 287грн</p></div>-->
      <!--<div class="scr-accessories__item&#45;&#45;green_button"  data-init="modal" data-modal="#callback-modal"><p>Подобрать</p></div>-->
    <!--</div>-->
  <!--</div>-->
<!--</section>-->



<section class="scr-sto__wrapper wrapper">
  <div class="scr-sto__inner inner">
    <div class="scr-sto__title">
      Сертифицированное СТО "Motul" в Харькове
    </div>
    <div class="scr-sto__subtitle">
      Полный спектр услуг по установке и сервису<br>
      <p style="color: #000;">Рынок ЛОСК, пл. Кононенко 1а</p>
    </div>
    <div class="slider_sto">
      <ul class="flip-items">
        <li>
          <img src="img/STO/1.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/2.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/3.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/4.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/sto1.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/sto.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/sto2.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/sto3.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/5.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/6.jpg" alt="">
        </li>
        <li>
          <img src="img/STO/7.jpg" alt="">
        </li>
      </ul>
    </div>
  </div>
</section>

<section class="scr-question__wrapper wrapper">
  <div class="scr-question__inner inner">
    <div class="scr-question__title">Остались вопросы?</div>
    <form  action="sendmessage.php" method="POST" class="scr-question__form">
      <div class="scr-question__form-title">
        Специалисты нашего колл-центра ответят на все интересующие вас вопросы, а также проконсультируют и помогут с подбором
      </div>
      <div class="scr-question__form-subtitle">
        Позвоните нам<br>
        +38(050) 323-27-40<br>
        или<br>
      </div>
      <div class="scr-question__form-input-wrapper">
        <input type="text" class="scr-question__form-input" placeholder="Введите телефон" name="phone">
      </div>
      <input type="hidden" name="order" value="Последний экран">
      <input type="hidden" name="tagmanager" value="/last-screen-form.html">
      <div class="scr-question__form-input-wrapper">
        <button class="scr-question__form-button" type="submit" data-content="Задать вопрос"></button>
      </div>
      <div class="scr-question__form-descr">Перезвоним в течении 5 минут</div>
    </form>
    <div class="scr-question__link"><img src="img/obves.png" alt=""><a href="http://ua-tuning.com/ ">Наш сайт с обвесами для защиты автомобиля.</a></div>
  </div>
</section>






  <div class="modal callback-modal" id="callback-modal" data-type="modal">
    <div class="modal__wrapper modal-wrapper">
      <span class="modal__close modal-close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        ЗАКАЗАТЬ<br>
        БЕСПЛАТНЫЙ<br>
        ЗВОНОК
      </span>
        <form action="sendmessage.php" method="POST">
          <div class="modal__input-wrapper">
            <label class="modal__label" for="callback-modal-text-1">Введите номер телефона</label>
            <input type="text" id="callback-modal-text-1" class="modal__input" name="phone" placeholder="+38 (___) ___-__-__">
            <input type="hidden" name="order" value="">
          </div>
          <div class="modal__input-wrapper">
            <button type="submit" class="modal__button" data-content="Отправить"></button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal response-modal" data-type="modal" id="response-modal">
    <div class="modal__wrapper">
      <span class="modal__close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        Сообщение отправлено!<br>
        Подберите кунг и получите скидку 10% на любой <a href="http://ua-tuning.com/">обвес из метала.</a>
      </span>
      </div>
    </div>
  </div>

  <script src="js/common.js"></script>
</body>

</html>
