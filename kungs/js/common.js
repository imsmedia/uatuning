function unserializeUrl(){
  var pairs = window.location.search;
  if (window.location.search=='') return '';
  var temp;
  var obj = {};
  pairs = pairs.substring(1).split('&');
  for (var i = 0; i < pairs.length; i++) {
    temp = pairs[i].split('=');
    obj[temp[0]] = temp[1];
  }
  return obj;
}
$(window).on('load', function () {
  var $preloader = $('#page-preloader'),
      $spinner   = $preloader.find('.spinner');
  $spinner.fadeOut();
  $preloader.delay(150).fadeOut('slow');
});
function Model() {
  this.events = {};
  this.callEvent = function(eventName, args){
    var eventArr = this.events[eventName];
    if (!!eventArr) {
      for (var i = 0; i < eventArr.length; i++) {
        eventArr[i].call(this, args ? args : undefined);
      }
    };
  };
  this.on = function(eventName, callback) {
    if (!this.events[eventName]) {
      this.events[eventName] = [];
    }
    this.events[eventName].push(callback.bind(this));
  };
  this.off = function(eventName) {
    if (!this.events[eventName]) return;
    this.events[eventName] = [];
  }
};
var events = new Model();

$.validator.addMethod('uaphone', function(value, element, params){
  var regex = /\+38 \(0[0-9]{2}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/g;
  var match = value.match(regex);
  if (match!==null) {
    if (match.length === 1) return true;
  }
  return false;
});
$('input[name="phone"]').mask('+38 (999) 999-99-99');

$(document).ready(function() {
  $.ajax({
    url: '/js/models.json',
    dataType: 'json'
  })
  .done(function(resp) {
    events.callEvent('model-recived', resp);
  });
  events.on('model-recived', function(data){
    var inputs = $('[name="auto"]').not('[id="carInput"]').get();
    for (var i = 0; i < inputs.length; i++) {
      $(inputs[i]).attr('autocomplete','off').livesearch(data);
    }
  })
  var utm = unserializeUrl();
  var title = document.querySelector('#title');
  var back = document.querySelector('#back');
  var multiland = {
    'kung_ford_ranger': {
      title: 'Добавьте преимуществ вашему FORD RANGER с надежными',
      img: '0005.jpg'
    },
    'kung_great_wall': {
      title: 'Добавьте преимуществ вашему GREAT WALL WINGLE с надежными',
      img: '0006.jpg'
    },
    'kung_mitsubishi_l200': {
      title: 'Добавьте преимуществ вашему MITSUBISHI L200 с надежными',
      img: '0008.jpg'
    },
    'kung_nissan_navara': {
      title: 'Добавьте преимуществ вашему NISSAN NAVARA с надежными',
      img: '0007.jpg'
    },
    'kung_nissan_np300': {
      title: 'Добавьте преимуществ вашему NISSAN NP-300 с надежными',
      img: '0003.jpg'
    },
    'kung_ssang_yong_action_sport': {
      title: 'Добавьте преимуществ вашему SSANGYONG ACTION SPORT с надежными',
      img: '0004.jpg'
    },
    'kung_toyot_hilux': {
      title: 'Добавьте преимуществ вашему TOYOTA HILUX с надежными',
      img: '0001.jpg'
    },
    'kung_volkswagen_amarok': {
      title: 'Добавьте преимуществ вашему VOLKSWAGEN AMAROK с надежными',
      img: '0002.jpg'
    }
  };
  if (typeof utm === 'object') {
    if(multiland[utm['utm_campaign']]) {
      var data = multiland[utm['utm_campaign']];
      title.innerHTML = data.title;
      back.style.backgroundImage = 'url(img/multiland/'+data.img+')';
    }
  }
  $.ajax({
        url: "api.php"
      })
      .done(function (data) {
        var obj = JSON.parse(data);
        events.callEvent('currencyLoaded', {currency: +obj[2].sale});
      });
  events.on('currencyLoaded', function(data){
    var cur = data.currency;
    $('.descr-box__new-price').each(function(){
      var price = $(this).attr('data-price');
      //для цены
      var lol = price*cur;
      var uaprice = /^[^.]+/.exec(lol)[0];//удалить числа после точки
      var probel = uaprice.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
      newuaprice = $(this).append('<p>от '+probel+' грн.</p>');
      // для старой цены
      var oldprice = uaprice*1.03;
      var probUaprice = /^[^.]+/.exec(oldprice)[0];//удалить числа после точки
      var oldprobel = probUaprice.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
      $(this).prev().text('от '+oldprobel+' грн.');
    });
  });


  var forms = document.getElementsByTagName('form');
  for (var i = 0; i < forms.length; i++) {
    $(forms[i]).validate({
      rules: {
        phone: {
          required: true,
          uaphone: true
        },
        email: {
          email:true,
          required:true
        }
      },
      messages: {
        phone: {
          required: 'Введите номер телефона',
          uaphone: 'Введите корректный номер телефона'
        },
        email: {
          required: 'Введите E-mail',
          email: 'Введите корректный Email'
        }
      },
      submitHandler: submitForm
    });
  };
  $('[data-init="modal"]').on('click', function() {
    var selector = $(this).attr('data-modal');
    var order = $(this).attr('data-order');
    var hiddenValue = $(this).attr('data-hidden');
    var modal = document.querySelector(selector);
    var form = modal.querySelector('form');
    var removed = form.querySelector('input[name="tagmanager"]');
    if (removed) removed.remove();
    var hidden = document.createElement('input');
    hidden.setAttribute('type', 'hidden');
    hidden.setAttribute('name', 'tagmanager');
    hidden.value = hiddenValue;
    form.appendChild(hidden);
    $(modal).addClass('active').find('[name="order"]').val(order);
  });
  $('.modal-close').on('click', function() {
    $(this).parent().parent().removeClass('active');
  });
  $('[data-type="modal"]').on('click', function() {
    $(this).removeClass('active');
  });
  $('.modal-wrapper').on('click', function(event) {
    event.stopPropagation();
  });
  $('.fancybox').fancybox({
    padding:0,
  });
  function submitForm(form, e){
    var data = $(form).serialize();
    var text = $(form).find('button').attr('data-content');
    var page = $(form).find('[name="tagmanager"]').val();
    $.ajax({
          url: 'sendmessage.php',
          type: 'POST',
          data: data,
          beforeSend: function(){
            $(form).find('input, button').attr('disabled', '');
            $(form).find('button').attr('data-content', 'Отправляем...');
          }
        })
        .done(function(response) {
          $(form).find('input, button').removeAttr('disabled');
          $(form).find('input[name="phone"]').val('');
          $(form).find('button').attr('data-content',text);
          $('[data-type="modal"]').removeClass('active');
          $('#response-modal').addClass('active');
          dataLayer.push({
            'event' : 'VirtualPageview',
            'virtualPageURL' : page,
            'virtualPageTitle' : page.replace('/', '')
          });
        })
        .fail(function(response) {
          console.log(response);
        });
  }


  $('#tabs').tabs();
  $('#tabs2').tabs();

  var carousel = $(".carousel").flipster({
    style: 'carousel',
    spacing: -0.5,
    nav: true,
    buttons:   true,
    scrollwheel: false,
    start: 0,
    loop: true
  });
  //добавление логотипов
  $("a.flipster__nav__link--category").each(function(){
    var carMark = $( this ).attr('data-flip-category');
    $(this).html('').append("<img src='img/logo/"+carMark+".png'>");
  });

  var carousel = $(".carousel2").flipster({
    style: 'carousel',
    spacing: -0.5,
    nav: false,
    buttons:   true,
    scrollwheel: false,
    start: 0,
    loop: true
  });

  $('.slider1').slick({
    draggable: true,
    autoplay:true,
    pauseOnHover: false,
    autoplaySpeed:1000
  });
  $('.slider_sklad').slick({
    draggable: true,
    autoplay:true,
    dots: true,
    fade: true,
    autoplaySpeed:1500
  });
  $('.slider1').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    var nS = ".item"+(nextSlide+1);
    $(".scr-iii__col--item").removeClass("active");
    $(nS).addClass("active");
    //var numberItem = /^[^.]+/.exec(lol)[0];//удалить числа после точки
  });

  var slider_sto = $(".slider_sto").flipster({
    style: 'flat',
    spacing: -0.25,
    nav: false,
    buttons:   true,
    scrollwheel: false,
    loop: true
  });

});
